let hSlider, vSlider,imgSlider;// sliders
let p1,p2,p3;// paragraphs
let numScreen; // Screen number (e.g. 0 => welcome screen)
let cnv;// canva
let cWidth,cHeight,tSize;
let img;

// Main functions
function preload() {
  img = loadImage('https://upload.wikimedia.org/wikipedia/fr/thumb/c/c8/Twitter_Bird.svg/600px-Twitter_Bird.svg.png');
}
function setup() {
  cnv = createCanvas(400, 400);
  centerCanva();
  numScreen=0;
  // permanent UI
  boutonFS= createButton("Plein ecran");
  boutonFS.mousePressed(displayFS);
  create_ui0();
  // point 0,0 centre image
  imageMode(CENTER);
}
function draw() {
  background(220);
  if(numScreen==0){
    // Display one image
    image(img, width/2, height/2, imgSlider.value(),imgSlider.value());
  }
}
// UI functions
function create_ui0() {
  // sliders & texts
  p1 = createElement('p', 'Taille canva horizontale');
  p1.position(15,0);
  hSlider = createSlider(400, 3000, 400);
  hSlider.position(20, 30);
  p2 = createElement('p', 'Taille canva verticale');
  p2.position(15,40);
  vSlider = createSlider(400, 3000, 400);
  vSlider.position(20, 70);
  p3 = createElement('p', 'Taille cible');
  p3.position(15,80);
  imgSlider = createSlider(10, 500, 100);
  imgSlider.position(20, 110);
  hSlider.input(resizeWindow); //event listener sliders
  vSlider.input(resizeWindow);
  //buttons
  boutonFS.position(200,20);// Fullscreen button
  boutonFillC= createButton("pleine largeur");// Fill canva => screen
  boutonFillC.position(200,40)
  boutonFillC.mousePressed(fillCanva);
  boutonReset= createButton("reset");// reset button
  boutonReset.position(200,60)
  boutonReset.mousePressed(resetCanva);
  boutonStart = createButton("Commencer"); // Start button
  boutonStart.position(20,150)
  boutonStart.mousePressed(start);

}
// Display functions
function displayFS(){
  let fs = fullscreen();
    fullscreen(!fs);
}
function resizeWindow() {
  resizeCanvas(hSlider.value(), vSlider.value());
  centerCanva();
}

function centerCanva(){
  cnv.position(windowWidth/2-width/2,windowHeight/2-height/2);
}
function removeUI(){
  // remove elements
  hSlider.remove();
  vSlider.remove();
  imgSlider.remove();
  boutonStart.remove();
  boutonFillC.remove();
  boutonReset.remove();
  p1.remove();
  p2.remove();
  p3.remove();
}
// Button functions
function start(){
  // sizes
  tSize=imgSlider.value();
  removeUI();
  numScreen=1;
  boutonRetour = createButton("Retour"); //come back button
  boutonRetour.position(20,20)
  boutonRetour.mousePressed(retour);
  boutonFS.position(80,20);
}
function resetCanva(){
  resizeCanvas(400, 400);
  centerCanva();
  removeUI();
  create_ui0();
}
function retour(){
  boutonRetour.remove();
  numScreen=0;
  create_ui0();
}
function fillCanva(){
  resizeCanvas(windowWidth, windowHeight);
  cnv.position(0,0);
}
